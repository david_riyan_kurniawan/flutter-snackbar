import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('snackbar'),
      ),
      body: Center(
        child: ElevatedButton(
            onPressed: () {
              /*
              membuat sebuah snackbar

              */
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //didalam widget snackbar kita bisa menambahkan beberapa fitur

                action: SnackBarAction(
                  label: 'cancel',
                  onPressed: () {},
                  textColor: Colors.black,
                ),
                content: Text('Delete profil sukses'),
                duration: Duration(seconds: 1),
                backgroundColor: Colors.blueAccent,
              ));
            },
            child: Text('tekan ini')),
      ),
    );
  }
}
